class PreNoel {

    static dbName (){
        return 'db-pre-noel.db'
    }
    static dropAllTables (db){

        db.run('DROP TABLE USERS')
        db.run('DROP TABLE PARTICIPANTS')
    }

    static dropUsersTable (db){

        db.run('DROP TABLE USERS')
    }

    static dropParticipantsTable (db){

        db.run('DROP TABLE PARTICIPANTS')
    }

    static creataAllTables (db) {

         db.run(`CREATE TABLE USERS (
             user_id INTEGER PRIMARY KEY AUTOINCREMENT,
             first_name TEXT,
             last_name TEXT,
             email TEXT,
             password TEXT,
             used BIT 
         )`)
        
        db.run(`CREATE TABLE PARTICIPANTS (
            participant_id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            pseudo TEXT,
            selected BIT  
        )`)
    }

    static createUserTable (db){

        db.run(`CREATE TABLE USERS (
            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name TEXT,
            last_name TEXT,
            email TEXT,
            password TEXT,
            used BIT,
            CONSTRAINT email_unique UNIQUE (email)    
        )`)
    }

    static createParticipantsTable (db){
        
        db.run(`CREATE TABLE PARTICIPANTS (
            participant_id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            pseudo TEXT,
            selected BIT  
        )`)
    }

    static seedAllTables (db){

        let insert1 = 'INSERT INTO USERS(first_name, last_name, email, password, used) VALUES(?,?,?,?,?)'
        
        db.run(insert1, ["Estelle", "", "", "Molly", 0])
        db.run(insert1, ["Olivier", "", "", "oli", 0])
        db.run(insert1, ["Kirikou", "", "", "spw", 0])
        db.run(insert1, ["Kim", "", "", "patisserie", 0])
        db.run(insert1, ["Damien", "", "", "camion", 0])
        db.run(insert1, ["popo", "", "", "arsouille", 0])
        db.run(insert1, ["Kolec", "", "", "rhumCoca", 0])
        db.run(insert1, ["Clem", "", "", "chevalierCovid", 0])
        db.run(insert1, ["Elora", "", "", "aveyron", 0])
        db.run(insert1, ["Mathieu", "", "", "manga", 0])
        db.run(insert1, ["Jeremy", "", "", "fishMan", 0])
        db.run(insert1, ["Camille", "", "", "gilles", 0])
        db.run(insert1, ["Gilles", "", "", "ricard", 0])
        db.run(insert1, ["Mario", "", "", "corleone", 0])
        db.run(insert1, ["Denis", "", "", "frero", 0])
        db.run(insert1, ["Nathan", "", "", "steam", 0])

        let insert = 'INSERT INTO PARTICIPANTS (name, pseudo, selected) VALUES(?,?,?)'
        
        db.run(insert, ["Olivier", "", 0])
        db.run(insert, ["Estelle", "", 0])
        db.run(insert, ["Kirikou", "", 0])
        db.run(insert, ["Kim", "", 0])
        db.run(insert, ["Damien", "", 0])
        db.run(insert, ["Popo", "", 0])
        db.run(insert, ["Kolec", "", 0])
        db.run(insert, ["Clem", "", 0])
        db.run(insert, ["Elora", "", 0])
        db.run(insert, ["Mathieu", "", 0])
        db.run(insert, ["Jeremy", "", 0])
        db.run(insert, ["Camille", "", 0])
        db.run(insert, ["Gilles", "", 0])
        db.run(insert, ["Mario", "", 0])
        db.run(insert, ["Denis", "", 0])
        db.run(insert, ["Nathan", "", 0])
    }

    static seedUsersTable (db){

        let insert1 = 'INSERT INTO USERS(first_name, last_name, email, password, used) VALUES(?,?,?,?,?)'
        
        db.run(insert1, ["Estelle", "", "", "Molly", 0])
        db.run(insert1, ["Olivier", "", "", "oli", 0])
        db.run(insert1, ["Kirikou", "", "", "spw", 0])
        db.run(insert1, ["Kim", "", "", "patisserie", 0])
        db.run(insert1, ["Damien", "", "", "camion", 0])
        db.run(insert1, ["popo", "", "", "arsouille", 0])
        db.run(insert1, ["Kolec", "", "", "rhumCoca", 0])
        db.run(insert1, ["Clem", "", "", "chevalierCovid", 0])
        db.run(insert1, ["Elora", "", "", "aveyron", 0])
        db.run(insert1, ["Mathieu", "", "", "manga", 0])
        db.run(insert1, ["Jeremy", "", "", "fishMan", 0])
        db.run(insert1, ["Camille", "", "", "gilles", 0])
        db.run(insert1, ["Gilles", "", "", "ricard", 0])
        db.run(insert1, ["Mario", "", "", "corleone", 0])
        db.run(insert1, ["Denis", "", "", "frero", 0])
        db.run(insert1, ["Nathan", "", "", "steam", 0])
    }

    static seedParticipantsTable (db){

        let insert = 'INSERT INTO PARTICIPANTS (name, pseudo, selected) VALUES(?,?,?)'
        
        db.run(insert, ["Olivier", "", 0])
        db.run(insert, ["Estelle", "", 0])
        db.run(insert, ["Kirikou", "", 0])
        db.run(insert, ["Kim", "", 0])
        db.run(insert, ["Damien", "", 0])
        db.run(insert, ["Popo", "", 0])
        db.run(insert, ["Kolec", "", 0])
        db.run(insert, ["Clem", "", 0])
        db.run(insert, ["Elora", "", 0])
        db.run(insert, ["Mathieu", "", 0])
        db.run(insert, ["Jeremy", "", 0])
        db.run(insert, ["Camille", "", 0])
        db.run(insert, ["Gilles", "", 0])
        db.run(insert, ["Mario", "", 0])
        db.run(insert, ["Denis", "", 0])
        db.run(insert, ["Nathan", "", 0])
    }

    static resetAllTables (db){

        let resetSelected = 'UPDATE PARTICIPANTS SET selected = 0'
        db.run(resetSelected)
    
        let resetUsed = 'UPDATE USERS SET used = 0'
        db.run(resetUsed)
    }

    static resetUsersTable (db){

        let resetSelected = 'UPDATE PARTICIPANTS SET selected = 0'
        db.run(resetSelected)
    }

    static resetParticipantsTable (db){

        let resetUsed = 'UPDATE USERS SET used = 0'
        db.run(resetUsed)
    }
    
}

module.exports = PreNoel;