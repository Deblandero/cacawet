class Deblander {

    static dbName (){
        return 'db-deb.db'
    }

    static dropAllTables (db){

        db.run('DROP TABLE USERS')
        db.run('DROP TABLE PARTICIPANTS')
    }

    static dropUsersTable (db){

        db.run('DROP TABLE USERS')
    }

    static dropParticipantsTable (db){

        db.run('DROP TABLE PARTICIPANTS')
    }

    static creataAllTables (db) {

         db.run(`CREATE TABLE USERS (
             user_id INTEGER PRIMARY KEY AUTOINCREMENT,
             first_name TEXT,
             last_name TEXT,
             email TEXT,
             password TEXT,
             used BIT 
         )`)
        
        db.run(`CREATE TABLE PARTICIPANTS (
            participant_id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            pseudo TEXT,
            selected BIT  
        )`)
    }

    static createUserTable (db){

        db.run(`CREATE TABLE USERS (
            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name TEXT,
            last_name TEXT,
            email TEXT,
            password TEXT,
            used BIT 
        )`)
    }

    static createParticipantsTable (db){
        
        db.run(`CREATE TABLE PARTICIPANTS (
            participant_id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            pseudo TEXT,
            selected BIT  
        )`)
    }

    static seedAllTables (db){

        let insert1 = 'INSERT INTO USERS(first_name, last_name, email, password, used) VALUES(?,?,?,?,?)'
        
        db.run(insert1, ["Estelle", "Gyetvai", "EstelleGtv@gmail.com", "Molly", 0])
        db.run(insert1, ["Olivier", "Deblander", "od@gmail.com", "oli", 0])
        db.run(insert1, ["Guy", "Deblander", "gd@gmail.com", "truelle", 0])
        db.run(insert1, ["Yvette", "Deblander", "yd@gmail.com", "tozzi", 0])
        db.run(insert1, ["Denis", "Deblander", "dd@gmail.com", "pumbastick", 0])
        db.run(insert1, ["Julie", "Deblander", "jd@gmail.com", "DenisTao", 0])
        db.run(insert1, ["Marie", "Deblander", "md@gmail.com", "mariadelmare", 0])
        db.run(insert1, ["Anthony", "Deblander", "ad@gmail.com", "whisky", 0])
        db.run(insert1, ["Tao", "Deblander", "td@gmail.com", "pompier", 0])
    
        let insert = 'INSERT INTO PARTICIPANTS (name, pseudo, selected) VALUES(?,?,?)'
        
        db.run(insert, ["Olivier", "", 0])
        db.run(insert, ["Estelle", "", 0])
        db.run(insert, ["Denis", "", 0])
        db.run(insert, ["Julie", "", 0])
        db.run(insert, ["Tao", "", 0])
        db.run(insert, ["Yvette", "", 0])
        db.run(insert, ["Guy", "", 0])
        db.run(insert, ["Marie", "", 0])
        db.run(insert, ["Anthony", "", 0])
    }

    static seedUsersTable (db){

        let insert1 = 'INSERT INTO USERS(first_name, last_name, email, password, used) VALUES(?,?,?,?,?)'
        
        db.run(insert1, ["Estelle", "Gyetvai", "EstelleGtv@gmail.com", "Molly", 0])
        db.run(insert1, ["Olivier", "Deblander", "od@gmail.com", "oli", 0])
        db.run(insert1, ["Guy", "Deblander", "gd@gmail.com", "truelle", 0])
        db.run(insert1, ["Yvette", "Deblander", "yd@gmail.com", "tozzi", 0])
        db.run(insert1, ["Denis", "Deblander", "dd@gmail.com", "pumbastick", 0])
        db.run(insert1, ["Julie", "Deblander", "jd@gmail.com", "DenisTao", 0])
        db.run(insert1, ["Marie", "Deblander", "md@gmail.com", "mariadelmare", 0])
        db.run(insert1, ["Anthony", "Deblander", "ad@gmail.com", "whisky", 0])
        db.run(insert1, ["Tao", "Deblander", "td@gmail.com", "pompier", 0])
    }

    static seedParticipantsTable (db){

        let insert = 'INSERT INTO PARTICIPANTS (name, pseudo, selected) VALUES(?,?,?)'
        
        db.run(insert, ["Olivier", "", 0])
        db.run(insert, ["Estelle", "", 0])
        db.run(insert, ["Denis", "", 0])
        db.run(insert, ["Julie", "", 0])
        db.run(insert, ["Tao", "", 0])
        db.run(insert, ["Yvette", "", 0])
        db.run(insert, ["Guy", "", 0])
        db.run(insert, ["Marie", "", 0])
        db.run(insert, ["Anthony", "", 0])
    }

    static resetAllTables (db){

        let resetSelected = 'UPDATE PARTICIPANTS SET selected = 0'
        db.run(resetSelected)
    
        let resetUsed = 'UPDATE USERS SET used = 0'
        db.run(resetUsed)
    }

    static resetUsersTable (db){

        let resetSelected = 'UPDATE PARTICIPANTS SET selected = 0'
        db.run(resetSelected)
    }

    static resetParticipantsTable (db){

        let resetUsed = 'UPDATE USERS SET used = 0'
        db.run(resetUsed)
    }
    
}

module.exports = Deblander;