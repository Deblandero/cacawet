module.exports = function (req, res, next) {

    if (req.session.user) {
        res.locals.user = req.session.user
    }
    
    req.user = function (data) {
        
        if (req.session.user === undefined) {
            req.session.user = {}
        }

        req.session.user.name = data.first_name
        req.session.user.used = data.used

    }

    next()
}