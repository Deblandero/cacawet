const { json } = require('express');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const router = express();
const port = 5000;

// EJS
router.set('view engine', 'ejs');

// Import statics files
router.use(express.static(__dirname +'/'));

// MIDDLEWARE
router.use(bodyParser.urlencoded({extended: false}))
router.use(bodyParser.json())
router.use(session({secret: 'Shh, c\'est secret',  resave: false, saveUninitialized: true, cookie: { secure: false }}));
router.use(require('./middlewares/flash'))
router.use(require('./middlewares/user'))

// DB
const db = require('./query');

// First route
router.get('/', (req, res) => {
    res.render('login');
})

router.post('/', (req, res) => {
    let login = require('./models/Login')
    
    login.signIn(req.body, (result) => {
        if (result == undefined) {
            console.log('Il y a un soucis de credentials')
            req.flash('error', 'Prénom et/ou mot de passe incorrect')
            res.redirect('/')   
        }else{
            req.flash('error', '')
            req.user(result)
            console.log(req.session.user.name + ' vient de se connecter ')
            res.status(200).render('cacawet', {user: req.session.user.name, participantSelected: ''})
        }
    })
    
})

// Route to button for running the selection 
router.post('/participantSelected', (req, res) => {
    
    // Check if user has already played
    if (req.session.user.used == 1) {
        console.log(req.session.user.name + " vient d'essayer de rejouer")
        res.render('used', {user: req.session.user.name})
    }else {
        let user = require('./models/User')

        // Update user to used = 1 and get all informations of this user;
        user.updateUserUsed(req.session.user.name)     
        req.session.user.used = 1   

        const selectParticipants = "SELECT * FROM PARTICIPANTS WHERE selected = 0 AND name <> '" + req.session.user.name + "'";
    
        db.all(selectParticipants, (err, data) => {
            
            let participant = require('./models/Participant')
            if (err){
                throw err
            }
            
            if (data == undefined) {
                res.status(400).render('cacawete');
                return;
            }

            
            // Check if there are particpants available
            if (data.length != 0) {   
                
                // Créate a index random to select a participant
                let indexRandom = Math.floor(Math.random() * data.length);
                                 
                // Update the participants list with a random participants update to selected = 1
                participant.deleteParticipant(data[indexRandom].name)
                
                // Return view with the selection
                res.render('participantSelected', {participantSelected: data[indexRandom].name})
                
            }else{
                // Return view if the participants liste is empty
                res.render('participantSelected', {participantSelected: "Tous les participants ont été déjà sélectionnés"})
            }
            req.session.destroy
        })
    }
})

router.listen(port, () => {
    console.log('Server en écoute sur le port http://localhost:' + port);
})

module.exports = router